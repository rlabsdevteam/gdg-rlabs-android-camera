package org.rlabs.gdg.camera;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class Main extends Activity implements OnClickListener {
	static final int REQUEST_IMAGE_CAPTURE = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// Info: At a start of any Android Application is the 'OnCreate'
		// function. This is where apps begins their execution.
		super.onCreate(savedInstanceState);
		// You can to let it load a view
		setContentView(R.layout.main);
		// Info: We create our first function to do more code for us
		setupView();
	}

	private void setupView() {
		// This button button links what is on your screen, the button you see
		// with something called a Listener.
		// A listener is a class in java that wait for something to happens, but
		// you got to ask it to listen to something first.
		Button button1 = (Button) findViewById(R.id.button1);
		// Here were asking the listener to listen for a button press
		button1.setOnClickListener(this);
	}

	private void dispatchTakePictureIntent() {
		// Here you will see an action which Android intents on doing. It begin
		// by Opening an class Dedicated to taking picture which is in android
		// built in libraries.
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
			// This is where the user decides to save the image to storage.
			startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
		}
	}

	@Override
	public void onClick(View v) {
		// After registering the Button below to listen when a button is click
		// we can tell what Android should do next.
		dispatchTakePictureIntent();
	}

	// Here is where android comes back and decides what do with the image which
	// is saved.
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
			// The MediaStore now send the details from Android to your code.
			Bundle extras = data.getExtras();
			// We will take that information and save to a something called
			// 'Bitmap'. Simply put Bitmaps can be use to temporary store image
			// files.
			Bitmap imageBitmap = (Bitmap) extras.get("data");

			// Since Java as a representation of the image you taken it need to
			// display it somewhere.
			// We register our ImageView we had on our XML, and
			ImageView imageview1 = (ImageView) findViewById(R.id.imageview1);
			// Change from a blank canvas and swop it out with our above image
			// stored in 'Bitmap' and place it into our ImageView
			imageview1.setImageBitmap(imageBitmap);
		}
	}

}
